'''
OOP GUI
Creator: Yiqing Liang
Create Date: 12.2.2014
'''

from Tkinter import *
from tkFileDialog import askopenfilename,askdirectory 
import sys
from math import sin, cos, sqrt, pi
from cv2 import cv
import urllib2
import cv2
import numpy
import os

#rootdir='./pic/'
#rootdir='/home/pi'
rootdir='./'

#some global value,
file = 'wire_rope_failure.jpg'
drag_start = None
scale_flag = False
sel = (0,0,0,0)


class Application(Frame):
    
    def __init__(self, master): # could input more argue for function handler
        Frame.__init__(self, master)
        self.startFunc = startDetect
        self.file = fileSelect
        self.scale = scaleFile
        self.startall = startDetectall
        self.folder = dicSelect
        self.initUI()
        
    def initUI(self):
        #main gird
        self.grid()
        
        #threshold scaler
        self.threshold_scale = Scale( self,label = 'threshold',orient=HORIZONTAL,length= 300, from_=1, to=300)
        self.threshold_scale.grid(row=0, column=0)

        #min dot scaler
        self.mindot_scale = Scale( self, label = 'min dot',orient=HORIZONTAL,length= 400, from_=50, to=6000)
        self.mindot_scale.grid(row=2, column=0)
        
        #max dot scaler
        self.maxdot_scale = Scale( self,label = 'max dot', orient=HORIZONTAL,length= 400, from_=800, to=20000)
        self.maxdot_scale.grid(row=4, column=0)
        
        #choose file button
        self.button_file = Button(root, text="select input file", command=self.file)
        self.button_file.grid(row=6, column=0)

        self.label_file = Label(root,text=file)
        self.label_file.grid(row=7, column=0)

        #choose dictionary button
        self.button_dic = Button(root, text="select folder ", command=self.folder)
        self.button_dic.grid(row=12, column=0)

        #scale image button
        self.button_scale = Button(root, text="scale source image region", command=self.scale)
        self.button_scale.grid(row=10, column=0)

        #destroy window button
        self.button_destroy = Button(root, text="destroy all window", command=self.closeAllWindows)
        self.button_destroy.grid(row=8, column=0)

        #start detect button
        self.button_start = Button(self, text="detect one", command=self.startFunc)
        self.button_start.grid(row=6, column=2)

        #start detect all image
        self.button_startall = Button(self, text="detect all", command=self.startall)
        self.button_startall.grid(row=7, column=2)


    def setLabelFileText(self,string):
        self.label_file['text'] = string

    def closeAllWindows(self):
        cv2.destroyAllWindows()
        


def scaleFile():
    img = cv2.imread(file)
    global sel,drag_start
    sel = (0,0,0,0)
    drag_start = None
    gray=cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    cv2.namedWindow("pre-scale",1)
    cv2.setMouseCallback("pre-scale", onmouse,gray) # last option arguement is input userdata.
    cv2.imshow("pre-scale",gray)


'''
Mouse click callback
'''
def onmouse(event, x, y, flags, param):
    global drag_start, sel,file,scale_flag
    gray = param
    if event == cv2.EVENT_LBUTTONDOWN:
        drag_start = x, y
        sel = 0,0,0,0
    elif event == cv2.EVENT_LBUTTONUP:
        if sel[2] > sel[0] and sel[3] > sel[1]:
            print sel
            scale_flag = True
            #patch = gray[sel[1]:sel[3],sel[0]:sel[2]]
            #cv2.imshow("scale",patch)
            #cv2.imwrite('scale.jpg',patch)
            #file = 'scale.jpg'
        drag_start = None
    elif drag_start:
        #print flags
        if flags & cv2.EVENT_FLAG_LBUTTON:
            minpos = min(drag_start[0], x), min(drag_start[1], y)
            maxpos = max(drag_start[0], x), max(drag_start[1], y)
            sel = minpos[0], minpos[1], maxpos[0], maxpos[1]
            img = cv2.cvtColor(gray, cv2.COLOR_GRAY2BGR)
            cv2.rectangle(img, (sel[0], sel[1]), (sel[2], sel[3]), (0,255,255), 1)
            cv2.imshow("pre-scale", img)
        else:
            print "selection is complete"
            drag_start = None


def startDetect():
    global scale_flag
    print app.threshold_scale.get(),file
    detectDamage("", file,scale_flag)
    scale_flag = False

def startDetectall():
    global scale_flag
    for subdir, dirs, files in os.walk(rootdir):
       for file in files:
           if (file.find("tif")>0):
              print file
              detectDamage(rootdir,file,scale_flag)
              
    scale_flag = False

def detectDamage(rootdir, filename,scale_flag = False):
    global sel
    #cv2.destroyAllWindows()
    
    #be black and white
    im_gray1 = cv2.imread(rootdir+filename, cv2.CV_LOAD_IMAGE_GRAYSCALE)
    (thresh, im_bw) = cv2.threshold(im_gray1, 128, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)
    cv2.imwrite('bw_image.png', im_bw)
    src = cv.LoadImage('bw_image.png', cv.CV_LOAD_IMAGE_GRAYSCALE)

    
    #src = cv.LoadImage(rootdir+filename, cv.CV_LOAD_IMAGE_GRAYSCALE)
    if scale_flag == True:
       src = src[sel[1]:sel[3],sel[0]:sel[2]]
       
    #src = cv.LoadImage(filename, cv.CV_LOAD_IMAGE_GRAYSCALE)
    cv.NamedWindow("Source", 1)
    cv.ShowImage("Source", src)
    cv.NamedWindow("Detected", 1)
    	
    dst = cv.CreateImage(cv.GetSize(src), 8, 1)
    color_dst = cv.CreateImage(cv.GetSize(src), 8, 3)
    
    storage = cv.CreateMemStorage(0)
    lines = 0
    cv.Canny(src, dst, 50, 200, 3) #300 600
    cv.CvtColor(dst, color_dst, cv.CV_GRAY2BGR)
	
    (mu, sigma) = cv2.meanStdDev(numpy.asarray(cv.GetMat(src)))	# TEST CODE
    #edges = cv.Canny(dst, mu - sigma, mu + sigma, 3)			# TEST CODE
    edges = dst # src ?
    cv.Canny(src, edges, mu - sigma, mu + sigma ,3)
    #draw edge
    cv.NamedWindow("edge", 1)
    cv.ShowImage("edge", edges)
    
    # get data from scaler,related to UI
    threshold_v = int(app.threshold_scale.get())
    '''
    lines = cv.HoughLines2(edges, storage, cv.CV_HOUGH_PROBABILISTIC, 1, pi / 180, threshold_v, 0, 0)
    for line in lines:
      w= 50
      cv.Line(color_dst, line[0], line[1], cv.CV_RGB(255, 0, 0), w, 8)
    '''
    maxX0 = 0
    minX0 = 10000
    (w,h) = cv.GetSize(src)
    lines = cv.HoughLines2(edges, storage, cv.CV_HOUGH_STANDARD, 0.1, pi/160, threshold_v,0,0 ) # 50, 10
    for (rho, theta) in lines[:200]:  
        #if theta > pi/20 and theta < pi/150:
        #    continue
        a = cos(theta)
        b = sin(theta)
        x0 = a * rho
        y0 = b * rho
        if (x0 < minX0) and x0 > 100:
          minX0 = x0
        if (x0 > maxX0) and x0 < (w-100):
          maxX0 = x0
        pt1 = (cv.Round(x0 + 1000*(-b)), cv.Round(y0 + 1000*(a)))
        pt2 = (cv.Round(x0 - 1000*(-b)), cv.Round(y0 - 1000*(a)))
        #w2=2 # 50
        #cv.Line(color_dst, pt1, pt2, cv.RGB(0,255, 255), w2,8)
    if maxX0 != 0 and minX0 != 10000 :
      print minX0, maxX0
      cv.Rectangle(color_dst, ( int(minX0),0), (int(maxX0),cv.GetSize(src)[0]), cv.RGB(0,255, 0), cv.CV_FILLED ,8)
    if int(minX0)-100 and int(maxX0)+100 :
      cv.Rectangle(color_dst, ( 0,0), (int(minX0)-100,h), cv.RGB(0,0, 0), cv.CV_FILLED ,8)
      cv.Rectangle(color_dst, ( int(maxX0)+100,0), (w,h), cv.RGB(0,0, 0), cv.CV_FILLED ,8)
    cv.ShowImage("Detected", color_dst)
		
#Test code
    cv.SaveImage('Source.jpg', src)
    cv.SaveImage('Detected.jpg',color_dst)
    
    tst = cv.LoadImage('Detected.jpg', cv.CV_LOAD_IMAGE_GRAYSCALE)
		
    damage = cv.CountNonZero(tst)
    # Detection based on detected non zero pixel amount
    print damage
    #gui related
    maxDot = int(app.maxdot_scale.get())
    minDot = int(app.mindot_scale.get())
    
    #if damage>150 and damage<800:
    if damage>minDot and damage<maxDot:
        # binary AND original AND detected damage
        t1=cv2.imread('Source.jpg')
        t2=cv2.imread('Detected.jpg')
        res = cv2.bitwise_or(t1,t2)
        #cv2.imshow("sum",res)
        cv2.imwrite('Summary.jpg', res) # do not comment it
	# new green Mat
        #sum = cv.CreateImage(res.size(), CV_8UC3, cv.Scalar(0,255,0));
        # set pixels using detected masked by blackWhite to blue
        #sum.setTo(Scalar(255,0,0), t2);	
        cres = cv.LoadImage('Summary.jpg')
        cv.SaveImage('Summary.jpg', cres)
        #res = cv2.imread('Summary.jpg')
        #re = numpy.vstack((t1,t2, res)) # vertical stack images
        re = numpy.hstack((t1,t2, res))  # horizontal stack images
        cv2.imwrite("result.jpg",re)
        cv2.imshow("Summary",re)
        #tmp = cv.LoadImage("result.jpg")
        #cv.NamedWindow("Summary", 1)
        #cv.ShowImage("Summary", tmp) # LOOK also cres
        #cv.ShowImage("Source", src)
        #cv.ShowImage("Detected", color_dst)
        cv.WaitKey(10)
        os.remove('Source.jpg')
        os.remove('Detected.jpg')
        os.remove('Summary.jpg')
        os.remove('bw_image.png')
        print "Detect damage"
    else:
        print "No damage"

def fileSelect():
    global file
    file = askopenfilename()
    #app.setLabelFileText(file)
    app.label_file['text'] = file
    print file

def dicSelect():
    global rootdir
    rootdir = askdirectory()
    app.label_file['text'] = rootdir
    rootdir = rootdir + '/'
    print rootdir

if __name__ == "__main__":
    #ui
    root = Tk() # create a window
    #modify root window
    root.title("Rope damage detector")
    root.geometry("500x500")

    app = Application(root)
    # event loop
    root.mainloop()



