#!/usr/bin/python
import sys
from math import sin, cos, sqrt, pi
import urllib2
import os
import numpy as np
import cv2
rootdir='./rop/srcImg/'


#cv2.startWindowThread()



def detectDamage(rootdir, filename):
    
    im = cv2.imread(rootdir+filename)
    imgray = cv2.cvtColor(im,cv2.COLOR_BGR2GRAY)
    ret,thresh2 = cv2.threshold(imgray,128,255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)
    thresh = (255-thresh2) #revert black and white
    contours, hierarchy = cv2.findContours(thresh,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)

    #cv2.drawContours(im,contours,-1,(0,255,0),3)

    edges = cv2.Canny(thresh2, 80, 120) 
    #cv2.imshow("edge",edges)

    for h,cnt in enumerate(contours):

        if cv2.contourArea(cnt) < 200: #filter some small dots.
            continue
        approx = cv2.approxPolyDP(cnt,0.3*cv2.arcLength(cnt,True),True)# remove small curves
        x,y,w,h = cv2.boundingRect(approx)
        cv2.rectangle(edges,(x-10,y),(x+w+10,y+h),(0,0,0),cv2.cv.CV_FILLED ) #cv2.cv.CV_FILLED   
        
    damage = cv2.countNonZero(edges)
    print damage

    #cv2.imshow("result", edges)

    if damage > 100 and damage < 3000:

      re = np.hstack((thresh2,imgray,edges))
      #cv2.imshow("detected",re)
      cv2.imwrite('./rop/damaged/'+filename,re)


if __name__ == "__main__":
    for subdir, dirs, files in os.walk(rootdir):
       for file in files:
           if (file.find("jpg")>0):
              print file
              detectDamage(rootdir,file)

            	
#for display imshow no bug on win7  , Yiqing
#cv2.waitKey(0) ## Wait for keystroke
#cv2.destroyAllWindows() ## Destroy all window

