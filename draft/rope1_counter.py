import numpy as np
import cv2


cv2.startWindowThread()
#input_img = '30-7-2014-2.jpg'
#input_img = '30-7-2014-2.jpg'
input_img = '1-8-2014-1.jpg'

im = cv2.imread(input_img)
imgray = cv2.cvtColor(im,cv2.COLOR_BGR2GRAY)
ret,thresh2 = cv2.threshold(imgray,128,255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)
thresh = (255-thresh2) #revert black and white
contours, hierarchy = cv2.findContours(thresh,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)

#cv2.drawContours(im,contours,-1,(0,255,0),3)

edges = cv2.Canny(thresh2, 80, 120) 
#cv2.imshow("edge",edges)

for h,cnt in enumerate(contours):

    if cv2.contourArea(cnt) < 200: #filter some small dots.
        continue
    approx = cv2.approxPolyDP(cnt,0.3*cv2.arcLength(cnt,True),True)# remove small curves
    x,y,w,h = cv2.boundingRect(approx)
    cv2.rectangle(edges,(x-20,y),(x+w+20,y+h),(0,0,0),-1 ) #cv2.cv.CV_FILLED   
    
damage = cv2.countNonZero(edges)
print damage

#cv2.imshow("result", edges)

if damage > 100 and damage < 3000:

  re = np.hstack((thresh2,imgray,edges))
  cv2.imshow("detected",re)

  
#for display imshow no bug on win7  , Yiqing
cv2.waitKey(0) ## Wait for keystroke
cv2.destroyAllWindows() ## Destroy all window

