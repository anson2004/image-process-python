#!/usr/bin/python
import sys
from math import sin, cos, sqrt, pi
from cv2 import cv
import urllib2
import cv2
import numpy
import os
rootdir='./rop/srcImg/'
#rootdir='/home/pi'

def countBlackPixels(grayImg):
    (w,h) = cv.GetSize(grayImg)
    size = w * h
    return size - cv.CountNonZero(grayImg)
	
def detectDamage(rootdir, filename):
    # toggle between CV_HOUGH_STANDARD and CV_HOUGH_PROBILISTIC
    USE_STANDARD = True
    im_gray1 = cv2.imread(rootdir+filename, cv2.CV_LOAD_IMAGE_GRAYSCALE)
    (thresh, im_bw) = cv2.threshold(im_gray1, 128, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)
    cv2.imwrite('bw_image.png', im_bw)
    src = cv.LoadImage('bw_image.png', cv.CV_LOAD_IMAGE_GRAYSCALE)
    #src = cv.LoadImage(rootdir+filename, cv.CV_LOAD_IMAGE_GRAYSCALE)
    #cv.NamedWindow("Source", 1)
    #cv.NamedWindow("Detected", 1)
    #cv.NamedWindow("Summary", 1)
    	
    dst = cv.CreateImage(cv.GetSize(src), 8, 1)
    color_dst = cv.CreateImage(cv.GetSize(src), 8, 3)
    storage = cv.CreateMemStorage(0)
    lines = 0
    cv.Canny(src, dst, 50, 200, 3)
    cv.CvtColor(dst, color_dst, cv.CV_GRAY2BGR)
	
    (mu, sigma) = cv2.meanStdDev(numpy.asarray(cv.GetMat(src)))	# TEST CODE
    # edges = cv.Canny(dst, mu - sigma, mu + sigma, 3)			# TEST CODE
    edges = dst # src ?
    cv.Canny(src, edges, mu - sigma, mu + sigma, )
    lines = cv.HoughLines2(edges, storage, cv.CV_HOUGH_STANDARD, 0.1, pi / 180, 71,0,0)				# TEST CODE/ important code
    maxX0 = 0
    minX0 = 10000
    (w,h) = cv.GetSize(src)
    for (rho, theta) in lines[:100]:
        a = cos(theta)
        b = sin(theta)
        x0 = a * rho
        y0 = b * rho
        if (x0 < minX0) and x0 > 50:
          minX0 = x0
        if (x0 > maxX0) and x0 < (w-50):
          maxX0 = x0
        pt1 = (cv.Round(x0 + 1000*(-b)), cv.Round(y0 + 1000*(a)))
        pt2 = (cv.Round(x0 - 1000*(-b)), cv.Round(y0 - 1000*(a)))
        #w_l=50
        #cv.Line(color_dst, pt1, pt2, cv.RGB(0, 0, 0), w_l, 8)
    if maxX0 != 0 and minX0 != 10000 :
      #print minX0, maxX0
      cv.Rectangle(color_dst, ( int(minX0)-10,0), (int(maxX0)+10,cv.GetSize(src)[0]), cv.RGB(0,0, 0), cv.CV_FILLED ,8)
    # cover outside part to black , just remain the surround part of rope
    if int(minX0)-100 and int(maxX0)+100 :
      pass
      #cv.Rectangle(color_dst, ( 0,0), (int(minX0)-100,h), cv.RGB(0,0, 0), cv.CV_FILLED ,8)
      #cv.Rectangle(color_dst, ( int(maxX0)+100,0), (w,h), cv.RGB(0,0, 0), cv.CV_FILLED ,8)					

		
#Test code
    cv.SaveImage('Source.jpg', src)
    cv.SaveImage('Detected.jpg',color_dst)
    tst = cv.LoadImage('Detected.jpg', cv.CV_LOAD_IMAGE_GRAYSCALE)
		
    damage = cv.CountNonZero(tst)
# Detection based on detected non zero pixel amount
    print damage
#    if damage>150 and damage<400:
    if damage>500 and damage<8000:
        # binary AND original AND detected damage
        t1=cv2.imread('Source.jpg')
        t2=cv2.imread('Detected.jpg')
        res = cv2.bitwise_or(t1,t2)
        cv2.imwrite('Summary.jpg', res)
	# new green Mat
        #sum = cv.CreateImage(res.size(), CV_8UC3, cv.Scalar(0,255,0));
        # set pixels using detected masked by blackWhite to blue
        #sum.setTo(Scalar(255,0,0), t2);	
        cres = cv.LoadImage('Summary.jpg')
        cv.SaveImage('Summary.jpg', cres)
        res = cv2.imread('Summary.jpg')
        #re = numpy.vstack((t1,t2, res)) # vertical stack images
        re = numpy.hstack((t1,t2, res))  # horizontal stack images
        cv2.imwrite('./rop/damaged/'+filename,re)
        tmp = cv.LoadImage('./rop/damaged/'+filename)
        #cv.ShowImage("Summary", tmp) # LOOK also cres
        #cv.ShowImage("Source", src)
        #cv.ShowImage("Detected", color_dst)
        cv.WaitKey(10)
        #os.remove('Source.jpg')
        #os.remove('Detected.jpg')
        os.remove('Summary.jpg')
        #os.remove('bw_image.png')
    os.remove('Source.jpg')
    os.remove('Detected.jpg')
    os.remove('bw_image.png')
	
if __name__ == "__main__":
    for subdir, dirs, files in os.walk(rootdir):
       for file in files:
           if (file.find("jpg")>0):
              print file
              detectDamage(rootdir,file)
            	
